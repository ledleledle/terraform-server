terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_security_group" "public-sec" {
  name        = "public"
  description = "Public Server Security Group"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "Public"
    Description = "Public Server Security Group"
  }
}

resource "aws_instance" "public" {
  ami               = "ami-00ddb0e5626798373"
  instance_type     = "t2.medium"
  source_dest_check = false
  key_name          = "bruh"
  subnet_id         = "subnet-05b40422a2186ed88"
  private_ip        = "172.31.1.30"
  security_groups   = aws_security_group.public-sec.*.id
  tags = {
    Name = "public"
  }
  root_block_device {
    delete_on_termination = true
    encrypted             = false
    iops                  = 100
    volume_size           = 15
    volume_type           = "gp2"
  }
}

resource "aws_security_group" "node-sec" {
  count       = 2
  name        = "node-${count.index + 1}"
  description = "Node/Worker Security Group"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["172.31.1.30/32"]
  }
  ingress {
    from_port   = 3000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["172.31.1.30/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "Node-${count.index + 1}"
    Description = "Node/Worker Security Group"
  }
}

resource "aws_instance" "node" {
  ami                         = "ami-00ddb0e5626798373"
  instance_type               = "t2.small"
  associate_public_ip_address = false
  source_dest_check           = false
  key_name                    = "bruh"
  subnet_id                   = "subnet-441ba765"
  security_groups             = [element(aws_security_group.node-sec.*.id, count.index)]
  count                       = 2
  tags = {
    Name = "node-${count.index + 1}"
  }
    root_block_device {
    delete_on_termination = true
    encrypted             = false
    iops                  = 100
    volume_size           = 10
    volume_type           = "gp2"
  }
}

resource "aws_eip" "lb" {
  instance = aws_instance.public.id
}
